# Code Templating Approach

Where an organizational unit expects to deliver multiple independent applications through different teams but wants there to be some level of standardization and to avoid repetitively inventing the wheel, a code templating approach make sense.

Projects generally need
- a READ ME structure
- a structure for version information
- a structure for source, test source
- a linting capability
- a task to run unit tests
- a task to build packages
- a task to publish packages

It is tedious and difficult to standardize these things when there is no automated way to establish these things in a consistent way.

*Additional mechanisms include use of [archetypes]() and some focus on library development and consumption.*

The mechanisms to achieve templated code includes
- git service template capabilities
- bespoke code generators

## Git Service Template Capabilities

Repositories provides such as GitHub and GitLab offer template mechanisms.

Unlike a normal repository where one normally forks a repository to make use previous work a template repository allows one to instantiate a new repository from the template.

![](./images/github-template.png)
Git Hub Template Usage

![](./images/gitlab-template.png)
Git Lab Template Usage

## Utilize an established code generation pattern

There are online services and tooling specifically tailored to generating applications.

[Yeoman](https://yeoman.io/) is one such service that has been operating for a considerable time and provides code generators for multiple tech stacks.

## Bespoke Code Generators

The npm package manager provides the ability to run a repository package runner module through its `npx` command.
This is of course used to create a react boilerplate app.

`npx create-react-app my-first-react-app`

Examples of this approach abound:
- [React Create SCript](https://github.com/facebook/create-react-app) : react-create-scripts package explicity to layout projects in standard ways
- [Express Generator](https://expressjs.com/en/starter/generator.html) : express generator
- [Electrode](http://www.electrode.io/) : Walmart open source platform for applications

So it is possible to create a npm package which provides a generator interface to select from candidate project types , capture parameters and then build a project.
An example of this is here. [Project Generator](https://gitlab.com/phil8/projgenerator/)


## Comparing approaches

The template repository approach is quite limited compared to a bespoke code generator.

The bespoke code generator provides templated repositories but also supports 
- parameterization and 
- supports sub-application level generation.

Parameterization is the ability to ask questions and generate different code based on the answers.
Sub Application Level generators provide the ability to add to an existing code base in a standard way.  For example generate a new express route with CRUD operations for a new domain entity.



